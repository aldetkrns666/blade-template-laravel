<!DOCTYPE html>
<html>
<head>
    <title>Home</title>
</head>
<body>
    <div>
        <div class="">
          <h1>Garuda Cyber Institute</h1>
          <p><b>Jadilah Programmer Handal Bersama GC-INS</b></p>
          <p>Grow Together With Garuda Cyber Institute</p>
          <h2>Syarat Dan Ketentuan</h2>
        </div>
  
        <div class="">
          <ul>
            <li>Tamatan SMA/SMK</li>
            <li>Tamatan Perguruan Tinggi</li>
            <li>Pekerja IT</li>
            <li>Freelancer</li>
          </ul>
  
          <h2>Cara Bergabung</h2>
          <ol>
            <li>Kunjungi Website GC-INS</li>
            <li>Register</li>
            <li>Lakukan Pembayaran</li>
          </ol>
        </div>
      </div>
      <a href="{{ route('register') }}"><button type="submit">Daftar Sekarang</button></a>
</body>
</html>
